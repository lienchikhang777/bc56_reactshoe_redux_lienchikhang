import React, { Component } from 'react'
import { connect } from 'react-redux';
import { changeAmount, deleteCart } from '../actions/cart';

class CartItem extends Component {
    render() {
        const { name, price, image, amount } = this.props.data;
        return (
            <tr>
                <td>
                    <img src={image} width={200} alt="" />
                </td>
                <td>
                    {name}
                </td>
                <td>
                    <p>{price}</p>
                </td>
                <td>
                    <button
                        onClick={() => this.props.handleAmount(this.props.data, -1)}
                    >-</button>
                    <span>{amount}</span>
                    <button
                        onClick={() => this.props.handleAmount(this.props.data, 1)}
                    >+</button>
                </td>
                <td>
                    <button
                        className="btn btn-danger"
                        onClick={() => { this.props.handleDelete(this.props.data) }}
                    >delete</button>
                </td>
            </tr>
        )
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        handleAmount: (shoe, option) => {
            dispatch(changeAmount(shoe, option))
        },
        handleDelete: (shoe) => {
            dispatch(deleteCart(shoe))
        }
    }
}

const mapStateToProps = (state) => {
    return {
        cart: state.cartReducer.cart
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CartItem)
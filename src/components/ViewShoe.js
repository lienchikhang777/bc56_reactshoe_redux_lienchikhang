import React, { Component } from 'react'
import { connect } from 'react-redux'

class ViewShoe extends Component {

    render() {
        const { image } = this.props.shoeDetail;
        return (
            <div className='col-12'>
                <img src={image} alt="" width={300} />
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        shoeDetail: state.shoeReducer.shoe,
    }
}

export default connect(mapStateToProps)(ViewShoe)

import React, { Component } from 'react'
import { connect } from 'react-redux';
import { addToCart, viewShoe } from '../actions/shoe';

class ShoeItem extends Component {

    renderingUI = () => {
        const { name, price, image } = this.props.data;
        const { handleAddCart, handleView } = this.props;
        return (
            <div className="card text-left">
                <img className="card-img-top" src={image} alt />
                <div className="card-body">
                    <h4 className="card-title">{name}</h4>
                    <p className="card-text">{price}</p>
                </div>
                <div className="card-footer">
                    <button
                        className='btn btn-success'
                        onClick={() => handleAddCart(this.props.data)}
                    >Add</button>
                    <button
                        className='btn btn-primary'
                        onClick={() => handleView(this.props.data)}
                    >View</button>
                </div>
            </div>
        )
    }

    render() {
        return (
            <div className='col-3'>
                {this.renderingUI()}
            </div>
        )
    }
}

let mapDispatchToState = (dispatch) => {
    return {
        handleAddCart: (shoe) => {
            dispatch(addToCart(shoe))
        },
        handleView: (shoe) => {
            dispatch(viewShoe(shoe))
        }
    }
}

export default connect(null, mapDispatchToState)(ShoeItem);
import React, { Component } from 'react'
import { connect } from 'react-redux'
import CartItem from './CartItem'

class Cart extends Component {

    rendering = () => {
        return this.props.cart.map((cart) => {
            return <CartItem key={cart.id} data={cart} />
        })
    }

    render() {
        return (
            <div>
                <table className='table'>
                    <thead>
                        <th>Image</th>
                        <th>Name</th>
                        <th>Price</th>
                        <th>Quantity</th>
                        <th>Action</th>
                    </thead>
                    <tbody>
                        {this.rendering()}
                    </tbody>
                </table>
            </div>
        )
    }
}

let mapStateToProps = (state) => {
    return {
        cart: state.cartReducer.cart
    }
}

let mapDispatchToProps = (dispatch) => {
    return {
        handleAmount: () => {

        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Cart);

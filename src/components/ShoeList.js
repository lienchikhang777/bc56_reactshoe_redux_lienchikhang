import React, { Component } from 'react'
import { connect } from 'react-redux'
import ShoeItem from './ShoeItem'


class ShoeList extends Component {



    renderUI = () => {
        return this.props.shoes.map((shoe) => {
            return <ShoeItem key={shoe.id} data={shoe} />
        })
    }

    render() {
        return (
            <div className='row'>{this.renderUI()}</div>
        )
    }
}

let mapStateToProps = (state) => {
    return {
        shoes: state.shoeReducer.shoes
    }
}

export default connect(mapStateToProps)(ShoeList);
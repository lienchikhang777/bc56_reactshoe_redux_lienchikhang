import { combineReducers } from "redux";
import { shoeReducer } from "./shoe";
import cartReducer from "./cart";
const rootReducer = combineReducers({
    shoeReducer,
    cartReducer
})

export default rootReducer;
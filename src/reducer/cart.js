
const initialState = {
    cart: [],
}

const cartReducer = (state = initialState, { type, payload }) => {
    switch (type) {
        case 'ADD_TO_CART': {
            let cloneCart = [...state.cart]
            //kiem tra sản phẩm được chọn đã có trong giỏ hàng chưa
            let index = cloneCart.findIndex((item) => item.id === payload.id)
            if (index == -1) {
                let cloneShoe = { ...payload, amount: 1 }
                cloneCart = [...state.cart, cloneShoe];
            } else {
                cloneCart[index].amount++;
            }
            state.cart = cloneCart
            return {
                ...state // return về state mới => render lại
            }
        }
        case 'CHANGE_AMOUNT': {
            let cloneArr = [...state.cart];
            const index = cloneArr.findIndex((item) => item.id === payload.id);
            cloneArr[index].amount += payload.option
            if (cloneArr[index].amount < 1) {
                cloneArr.splice(index, 1);
            }
            return {
                ...state,
                cart: cloneArr
            }
        }
        case 'DELETE_CART': {
            let cloneArr = [...state.cart];
            let indexDelete = cloneArr.findIndex((item) => { return item.id === payload });
            cloneArr.splice(indexDelete, 1);
            state.cart = cloneArr;
            return {
                ...state
            }
        }
        default: {
            return state;
        }
    }
}

export default cartReducer
import { data } from '../components/data.js'

const initialState = {
    shoes: data,
    shoe: data[0],
}

export const shoeReducer = (state = initialState, { type, payload }) => {
    switch (type) {
        case 'VIEW_SHOE': {
            return {
                ...state,
                shoe: payload
            }
        }
        default: {
            return state;
        }
    }
}
export const addToCart = (shoe) => {
    return {
        type: 'ADD_TO_CART',
        payload: shoe,
    }
}

export const viewShoe = (shoe) => {
    return {
        type: 'VIEW_SHOE',
        payload: shoe,
    }
}
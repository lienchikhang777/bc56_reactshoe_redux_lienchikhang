export const changeAmount = (shoe, option) => {
    return {
        type: 'CHANGE_AMOUNT',
        payload: { ...shoe, option },
    }
}

export const deleteCart = (shoe) => {
    return {
        type: 'DELETE_CART',
        payload: shoe,
    }
}
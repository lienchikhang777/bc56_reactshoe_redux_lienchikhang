import logo from './logo.svg';
import './App.css';
import Cart from './components/Cart';
import ShoeList from './components/ShoeList';
import ViewShoe from './components/ViewShoe';

function App() {
  return (
    <div className="App">
      <div className="row">
        <div className="col-6">
          <Cart />
        </div>
        <div className="col-6">
          <ShoeList />
        </div>
      </div>
      <div className="row">
        <ViewShoe />
      </div>
    </div>
  );
}

export default App;
